﻿using System;
using System.Collections.Generic;
using System.Linq;
using SGPF.EntityDB;

namespace SGPF.Services
{
    public class EstadoDaoService : AbstractSingletonDao<EstadoDaoService>, IDaoService<Estado, int>
    {
        #region IDaoService<Estado,int> Members

        public IEnumerable<Estado> GetAll()
        {
            using (var ctx = new EAModelContainer())
            {
                IQueryable<Estado> data = from estados in ctx.EstadoSet select estados;
                return data.ToList();
            }
        }

        public IEnumerable<Estado> GetAllBetweenDates(DateTime effDate, DateTime expDate)
        {
            throw new NotImplementedException();
        }

        public Estado Read(int id)
        {
            using (var ctx = new EAModelContainer())
            {
                IQueryable<Estado> data = from estados in ctx.EstadoSet
                                          where estados.Id == id
                                          select estados;
                return data.FirstOrDefault();
            }
        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(Estado element)
        {
            throw new NotImplementedException();
        }

        public int Create(Estado element)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}