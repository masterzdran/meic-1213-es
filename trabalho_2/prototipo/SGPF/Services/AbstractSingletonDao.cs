﻿using System;
using System.Web.Mvc;

namespace SGPF.Services
{
    public abstract class AbstractSingletonDao<T> where T : new()
    {
        internal static T Instance;

        public static T GetInstance()
        {
            if (Instance == null) Instance = new T();
            return Instance;
        }

    }
}
