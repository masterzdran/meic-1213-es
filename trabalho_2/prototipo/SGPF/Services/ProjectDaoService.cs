﻿using System;
using System.Collections.Generic;
using System.Linq;
using SGPF.EntityDB;

namespace SGPF.Services
{
    public class ProjectDaoService : AbstractSingletonDao<ProjectDaoService>, IDaoService<Projecto, int>
    {
        #region IDaoService<Projecto,int> Members

        public IEnumerable<Projecto> GetAll(String descricao)
        {
            using (var ctx = new EAModelContainer())
            {
                var projectList = from projectos in ctx.ProjectoSet where projectos.DescricaoProjecto.Equals(descricao) select projectos ;
                return projectList.ToList();
            }
        }

        public IEnumerable<Projecto> GetAll()
        {
            using (var ctx = new EAModelContainer())
            {
                var projectList = from projectos in ctx.ProjectoSet select projectos;
                return projectList.ToList();
            }
        }

        public IEnumerable<Projecto> GetAllBetweenDates(DateTime effDate, DateTime expDate)
        {
            using (var ctx = new EAModelContainer())
            {
                IQueryable<Projecto> projectList = from projectos in ctx.ProjectoSet
                                                   where
                                                       projectos.DataCriacao >= effDate &&
                                                       projectos.DataCriacao <= expDate
                                                   select projectos;
                return projectList.ToList();
            }
        }

        public Projecto Read(int id)
        {
            using (var ctx = new EAModelContainer())
            {
                var projectList = from projecto in ctx.ProjectoSet
                                                   where projecto.Id == id
                                                   select projecto;
                return projectList.FirstOrDefault();
            }
        }

        public int Delete(int id)
        {
        /*
        using (var ctx = new EAModelContainer())
        {
            var projectList = from projecto in ctx.ProjectoSet
                              where projecto.Id == id
                              select projecto;
            foreach (var projecto in projectList)
            {
                ctx.ProjectoSet.Remove(projecto);
            }
            return projectList.Count();
        }
        */

            using (var ctx = new EAModelContainer())
            {
                var projectList = from projecto in ctx.ProjectoSet
                                                   where projecto.Id == id
                                                   select projecto;

                Projecto pj = projectList.FirstOrDefault();
                if (pj != null) pj.Estado.Valor = "Eliminado";

                return ctx.SaveChanges();
            }
        }

        public int Update(Projecto element)
        {
            int records = 0;

            if (element != null)
            {
                using (var ctx = new EAModelContainer())
                {
                    ctx.ProjectoSet.Attach(element);

                    ctx.ChangeTracker.DetectChanges();

                    ctx.SaveChanges();
                }
            }
            return records;

        }

        public int Create(Projecto element)
        {
            int records = 0;
            using (var ctx = new EAModelContainer())
            {
                ctx.ProjectoSet.Add(element);
                records = ctx.SaveChanges();
            }
            return records;
        }

        #endregion

    }
}