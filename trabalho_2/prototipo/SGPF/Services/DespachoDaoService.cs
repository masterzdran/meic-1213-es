﻿using System;
using System.Collections.Generic;
using System.Linq;
using SGPF.EntityDB;

namespace SGPF.Services
{
    public class DespachoDaoService : AbstractSingletonDao<DespachoDaoService>, IDaoService<Despacho, int>
    {

        #region IDaoService<Parecer,int> Members

        public IEnumerable<Despacho> GetAll()
        {
            using (var ctx = new EAModelContainer())
            {
                IQueryable<Anexo> data = from anexos in ctx.AnexoSet where anexos.GetType() == typeof(Despacho) select anexos;
                return data.Cast<Despacho>(); // ETC -> End of Thinking Capacity
            }
        }

        public IEnumerable<Despacho> GetAllBetweenDates(DateTime effDate, DateTime expDate)
        {
            throw new NotImplementedException();
        }

        public Despacho Read(int id)
        {
            using (var ctx = new EAModelContainer())
            {
                IQueryable<Anexo> data = from anexo in ctx.AnexoSet
                                         where anexo.Id == id && anexo.GetType() == typeof(Despacho)
                                         select anexo;
                return data.FirstOrDefault() as Despacho; // ETC -> End of Thinking Capacity
            }
        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(Despacho element)
        {
            int records = 0;
            using (var ctx = new EAModelContainer())
            {
                ctx.AnexoSet.Add(element);
                records = ctx.SaveChanges();
            }
            return records;
        }

        public int Create(Despacho element)
        {
            int records = 0;
            using (var ctx = new EAModelContainer())
            {
                ctx.AnexoSet.Add(element);
                records = ctx.SaveChanges();
            }
            return records;
        }

        #endregion
    }
}