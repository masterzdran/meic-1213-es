﻿using System;
using System.Collections.Generic;

namespace SGPF.Services
{
    public interface IDaoService<TType, TId>
    {
        IEnumerable<TType> GetAll();
        
        IEnumerable<TType> GetAllBetweenDates(DateTime effDate, DateTime expDate);

        TType Read(TId id);

        int Delete(TId id);

        int Update(TType element);

        int Create(TType element);
    }
}