﻿using System;
using System.Collections.Generic;
using SGPF.EntityDB;

namespace SGPF.Services
{
    internal interface IUserDaoService : IDaoService<Utilizador, int>
    {
        IEnumerable<Utilizador> GetAllAsRole(String role);
    }
}