﻿using System;
using System.Collections.Generic;
using System.Linq;
using SGPF.EntityDB;

namespace SGPF.Services
{
    public class ParecerDaoService : AbstractSingletonDao<ParecerDaoService>, IDaoService<Parecer, int>
    {

        #region IDaoService<Parecer,int> Members

        public IEnumerable<Parecer> GetAll()
        {
            using (var ctx = new EAModelContainer())
            {
                IQueryable<Anexo> data = from anexos in ctx.AnexoSet where anexos.GetType() == typeof(Parecer) select anexos;
                return data.Cast<Parecer>(); // ETC -> End of Thinking Capacity
            }
        }

        public IEnumerable<Parecer> GetAllBetweenDates(DateTime effDate, DateTime expDate)
        {
            throw new NotImplementedException();
        }

        public Parecer Read(int id)
        {
            using (var ctx = new EAModelContainer())
            {
                IQueryable<Anexo> data = from anexo in ctx.AnexoSet
                                         where anexo.Id == id && anexo.GetType()==typeof(Parecer)
                                         select anexo;
                return data.FirstOrDefault() as Parecer; // ETC -> End of Thinking Capacity
            }
        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(Parecer element)
        {
            int records = 0;
            using (var ctx = new EAModelContainer())
            {
                ctx.AnexoSet.Add(element);
                records = ctx.SaveChanges();
            }
            return records;
        }

        public int Create(Parecer element)
        {
            int records = 0;
            using (var ctx = new EAModelContainer())
            {
                ctx.AnexoSet.Add(element);
                records = ctx.SaveChanges();
            }
            return records;
        }

        #endregion
    }
}