﻿using System;
using System.Collections.Generic;
using System.Linq;
using SGPF.EntityDB;

namespace SGPF.Services
{
    public class PapelDaoService : AbstractSingletonDao<PapelDaoService>, IDaoService<Papel, int>
    {
        #region IDaoService<Papel,int> Members

        public IEnumerable<Papel> GetAll()
        {
            using (var ctx = new EAModelContainer())
            {
                IQueryable<Papel> data = from estados in ctx.PapelSet select estados;
                return data.ToList();
            }
        }

        public IEnumerable<Papel> GetAllBetweenDates(DateTime effDate, DateTime expDate)
        {
            throw new NotImplementedException();
        }

        public Papel Read(int id)
        {
            using (var ctx = new EAModelContainer())
            {
                IQueryable<Papel> data = from estados in ctx.PapelSet
                                         where estados.Id == id
                                         select estados;
                return data.FirstOrDefault();
            }
        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(Papel element)
        {
            int records = 0;
            using (var ctx = new EAModelContainer())
            {
                ctx.PapelSet.Add(element);
                records = ctx.SaveChanges();
            }
            return records;
        }

        public int Create(Papel element)
        {
            int records = 0;
            using (var ctx = new EAModelContainer())
            {
                ctx.PapelSet.Add(element);
                records = ctx.SaveChanges();
            }
            return records;
        }

        #endregion

    }
}