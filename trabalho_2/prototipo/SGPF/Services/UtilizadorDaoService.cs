﻿using System;
using System.Collections.Generic;
using System.Linq;
using SGPF.EntityDB;

namespace SGPF.Services
{
    public class UtilizadorDaoService : AbstractSingletonDao<UtilizadorDaoService>, IUserDaoService
    {
        #region IUserDaoService Members

        public IEnumerable<Utilizador> GetAll()
        {
            using (var ctx = new EAModelContainer())
            {
                IQueryable<Utilizador> elements = from s in ctx.UtilizadorSet
                                                  select s;
                return elements.ToList();
            }
        }

        public IEnumerable<Utilizador> GetAllAsRole(string role)
        {
            using (var ctx = new EAModelContainer())
            {
                IQueryable<Utilizador> elements = from s in ctx.UtilizadorSet
                                                  where s.Papel.Equals(role)
                                                  select s;
                return elements.ToList();
            }
        }

        public IEnumerable<Utilizador> GetAllBetweenDates(DateTime effDate, DateTime expDate)
        {
            throw new NotImplementedException();
        }

        public Utilizador Read(int id)
        {
            using (var ctx = new EAModelContainer())
            {
                IQueryable<Utilizador> elements = from s in ctx.UtilizadorSet
                                                  where s.Id == id
                                                  select s;
                return elements.FirstOrDefault();
            }
        }

        public int Delete(int id)
        {
            /*
        using (var ctx = new EAModelContainer())
        {
            var elements = from s in ctx.UtilizadorSet
                           where s.Id==id
                           select s;
            int count = 0;
            elements.ToList().ForEach( (usr)=>
                                           {
                                               ctx.UtilizadorSet.Remove(usr);
                                               ++count;
                                           } 
                                    );
            
            ctx.SaveChanges();
            
            return count;
        }
        */
            throw new NotImplementedException();
        }

        public int Update(Utilizador element)
        {
            throw new NotImplementedException();
        }

        public int Create(Utilizador element)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}