﻿using System;
using System.Collections.Generic;
using System.Linq;
using SGPF.EntityDB;

namespace SGPF.Services
{
    public class AnexoDaoService : AbstractSingletonDao<AnexoDaoService>, IDaoService<Anexo, int>
    {
        #region IDaoService<Anexo,int> Members

        public IEnumerable<Anexo> GetAll()
        {
            using (var ctx = new EAModelContainer())
            {
                IQueryable<Anexo> data = from anexos in ctx.AnexoSet select anexos;
                return data.ToList();
            }
        }

        public IEnumerable<Anexo> GetAllBetweenDates(DateTime effDate, DateTime expDate)
        {
            throw new NotImplementedException();
        }

        public Anexo Read(int id)
        {
            using (var ctx = new EAModelContainer())
            {
                IQueryable<Anexo> data = from anexo in ctx.AnexoSet
                                         where anexo.Id == id
                                         select anexo;
                return data.FirstOrDefault();
            }
        }

        public int Delete(int id)
        {
            throw new NotImplementedException();
        }

        public int Update(Anexo element)
        {
            int records = 0;
            using (var ctx = new EAModelContainer())
            {
                ctx.AnexoSet.Add(element);
                records = ctx.SaveChanges();
            }
            return records;
        }

        public int Create(Anexo element)
        {
            int records = 0;
            using (var ctx = new EAModelContainer())
            {
                ctx.AnexoSet.Add(element);
                records = ctx.SaveChanges();
            }
            return records;
        }

        #endregion
    }
}