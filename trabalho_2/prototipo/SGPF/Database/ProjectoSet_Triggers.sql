﻿/*
DROP TRIGGER [ProjectoSetUpdateLogTriggerInsert];
DROP TRIGGER [ProjectoSetUpdateLogTriggerUpdate];
DROP TRIGGER [ProjectoSetUpdateLogTriggerDelete];
*/


CREATE TRIGGER [ProjectoSetUpdateLogTriggerInsert]
	ON [dbo].[ProjectoSet]
	AFTER INSERT
	AS
	BEGIN
		INSERT INTO [dbo].[ProjectoLogSet] (Nome, Custo, MontanteFinanciamento, NIB, DescricaoProjecto, Projecto_Id, Estado_Id, Utilizador_Id, DataAlteracao) 
		SELECT Nome, Custo, MontanteFinanciamento, NIB, DescricaoProjecto, Id, Estado_Id, Utilizador_Id, GETDATE() FROM inserted
	END
	;
GO

CREATE TRIGGER [ProjectoSetUpdateLogTriggerUpdate]
	ON [dbo].[ProjectoSet]
	AFTER UPDATE
	AS
	BEGIN
		INSERT INTO [dbo].[ProjectoLogSet] (Nome, Custo, MontanteFinanciamento, NIB, DescricaoProjecto, Projecto_Id, Estado_Id, Utilizador_Id ,DataAlteracao) 
		SELECT Nome, Custo, MontanteFinanciamento, NIB, DescricaoProjecto, Id, Estado_Id, Utilizador_Id, GETDATE() FROM [dbo].[ProjectoSet]
	END
	;

GO
CREATE TRIGGER [ProjectoSetUpdateLogTriggerDelete]
	ON [dbo].[ProjectoSet]
	AFTER DELETE
	AS
	BEGIN
		INSERT INTO [dbo].[ProjectoLogSet] (Nome, Custo, MontanteFinanciamento, NIB, DescricaoProjecto, Projecto_Id, Estado_Id, Utilizador_Id, DataAlteracao) 
		SELECT Nome, Custo, MontanteFinanciamento, NIB, DescricaoProjecto, Id, Estado_Id, Utilizador_Id, GETDATE() FROM deleted
	END
	;
