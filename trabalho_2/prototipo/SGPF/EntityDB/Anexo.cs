//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SGPF.EntityDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class Anexo
    {
        public Anexo()
        {
            this.AnexoProjecto = new HashSet<AnexoProjecto>();
        }
    
        public int Id { get; set; }
        public string Descricao { get; set; }
    
        public virtual ICollection<AnexoProjecto> AnexoProjecto { get; set; }
    }
}
