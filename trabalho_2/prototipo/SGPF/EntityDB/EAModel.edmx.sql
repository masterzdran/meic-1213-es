
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, and Azure
-- --------------------------------------------------
-- Date Created: 01/29/2013 20:33:01
-- Generated from EDMX file: C:\Users\fabio\Repos\meic-1213-es\trabalho_2\prototipo\SGPF\EntityFW\EAModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [db];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------


-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'ProjectoSet'
CREATE TABLE [dbo].[ProjectoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nome] nvarchar(max)  NOT NULL,
    [Custo] decimal(18,0)  NOT NULL,
    [MontanteFinanciamento] decimal(18,0)  NOT NULL,
    [NIB] nvarchar(max)  NOT NULL,
    [DataCriacao] datetime  NOT NULL,
    [DescricaoProjecto] nvarchar(max)  NOT NULL,
    [Estado_Id] int  NOT NULL,
    [Utilizador_Id] int  NULL,
    [PromotorProjecto_Id] int  NOT NULL,
    [AnexoProjecto_Id] int  NOT NULL
);
GO

-- Creating table 'UtilizadorSet'
CREATE TABLE [dbo].[UtilizadorSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nome] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [Telefone] nvarchar(max)  NOT NULL,
    [Papel_Id] int  NOT NULL
);
GO

-- Creating table 'PapelSet'
CREATE TABLE [dbo].[PapelSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Valor] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PromotorSet'
CREATE TABLE [dbo].[PromotorSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Nacionalidade] nvarchar(max)  NOT NULL,
    [NIF] nvarchar(max)  NOT NULL,
    [Nome] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [Telefone] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'AnexoSet'
CREATE TABLE [dbo].[AnexoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Descricao] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'EstadoSet'
CREATE TABLE [dbo].[EstadoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Valor] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'PagamentoSet'
CREATE TABLE [dbo].[PagamentoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Valor] nvarchar(max)  NOT NULL,
    [Data] datetime  NOT NULL,
    [Estado] bit  NOT NULL,
    [Projecto_Id] int  NOT NULL
);
GO

-- Creating table 'RegistoDeContactoSet'
CREATE TABLE [dbo].[RegistoDeContactoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Descricao] nvarchar(max)  NOT NULL,
    [Utilizador_Id] int  NOT NULL,
    [Projecto_Id] int  NOT NULL
);
GO

-- Creating table 'ProjectoLogSet'
CREATE TABLE [dbo].[ProjectoLogSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DataAlteracao] datetime  NOT NULL,
    [Nome] nvarchar(max)  NOT NULL,
    [Custo] decimal(18,0)  NOT NULL,
    [MontanteFinanciamento] decimal(18,0)  NOT NULL,
    [NIB] nvarchar(max)  NOT NULL,
    [DescricaoProjecto] nvarchar(max)  NOT NULL,
    [Projecto_Id] int  NOT NULL
);
GO

-- Creating table 'PromotorProjectoSet'
CREATE TABLE [dbo].[PromotorProjectoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Promotor_Id] int  NOT NULL
);
GO

-- Creating table 'AnexoProjectoSet'
CREATE TABLE [dbo].[AnexoProjectoSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Anexo_Id] int  NOT NULL
);
GO

-- Creating table 'ProjectoSet_Incentivo'
CREATE TABLE [dbo].[ProjectoSet_Incentivo] (
    [NumeroDePrestacoesPagamento] int  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'ProjectoSet_Bonificação'
CREATE TABLE [dbo].[ProjectoSet_Bonificação] (
    [InicioPeriodo] datetime  NOT NULL,
    [FimPeriodo] datetime  NOT NULL,
    [MontanteMaximo] decimal(18,0)  NOT NULL,
    [PercentagemJuro] decimal(18,0)  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'AnexoSet_Despacho'
CREATE TABLE [dbo].[AnexoSet_Despacho] (
    [LimiteFinanciamento] decimal(18,0)  NOT NULL,
    [MontanteFinanciamento] decimal(18,0)  NOT NULL,
    [ResultadoDaAvaliacao] nvarchar(max)  NOT NULL,
    [InfoCustoElegivel] nvarchar(max)  NOT NULL,
    [PrazoExecucao] nvarchar(max)  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'AnexoSet_Parecer'
CREATE TABLE [dbo].[AnexoSet_Parecer] (
    [ParecerTecnico] nvarchar(max)  NOT NULL,
    [Decisao] nvarchar(max)  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- Creating table 'AnexoSet_Reforco'
CREATE TABLE [dbo].[AnexoSet_Reforco] (
    [Estado] bit  NOT NULL,
    [Id] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'ProjectoSet'
ALTER TABLE [dbo].[ProjectoSet]
ADD CONSTRAINT [PK_ProjectoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'UtilizadorSet'
ALTER TABLE [dbo].[UtilizadorSet]
ADD CONSTRAINT [PK_UtilizadorSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PapelSet'
ALTER TABLE [dbo].[PapelSet]
ADD CONSTRAINT [PK_PapelSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PromotorSet'
ALTER TABLE [dbo].[PromotorSet]
ADD CONSTRAINT [PK_PromotorSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AnexoSet'
ALTER TABLE [dbo].[AnexoSet]
ADD CONSTRAINT [PK_AnexoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EstadoSet'
ALTER TABLE [dbo].[EstadoSet]
ADD CONSTRAINT [PK_EstadoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PagamentoSet'
ALTER TABLE [dbo].[PagamentoSet]
ADD CONSTRAINT [PK_PagamentoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'RegistoDeContactoSet'
ALTER TABLE [dbo].[RegistoDeContactoSet]
ADD CONSTRAINT [PK_RegistoDeContactoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProjectoLogSet'
ALTER TABLE [dbo].[ProjectoLogSet]
ADD CONSTRAINT [PK_ProjectoLogSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PromotorProjectoSet'
ALTER TABLE [dbo].[PromotorProjectoSet]
ADD CONSTRAINT [PK_PromotorProjectoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AnexoProjectoSet'
ALTER TABLE [dbo].[AnexoProjectoSet]
ADD CONSTRAINT [PK_AnexoProjectoSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProjectoSet_Incentivo'
ALTER TABLE [dbo].[ProjectoSet_Incentivo]
ADD CONSTRAINT [PK_ProjectoSet_Incentivo]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ProjectoSet_Bonificação'
ALTER TABLE [dbo].[ProjectoSet_Bonificação]
ADD CONSTRAINT [PK_ProjectoSet_Bonificação]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AnexoSet_Despacho'
ALTER TABLE [dbo].[AnexoSet_Despacho]
ADD CONSTRAINT [PK_AnexoSet_Despacho]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AnexoSet_Parecer'
ALTER TABLE [dbo].[AnexoSet_Parecer]
ADD CONSTRAINT [PK_AnexoSet_Parecer]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AnexoSet_Reforco'
ALTER TABLE [dbo].[AnexoSet_Reforco]
ADD CONSTRAINT [PK_AnexoSet_Reforco]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [Papel_Id] in table 'UtilizadorSet'
ALTER TABLE [dbo].[UtilizadorSet]
ADD CONSTRAINT [FK_UtilizadorPapel]
    FOREIGN KEY ([Papel_Id])
    REFERENCES [dbo].[PapelSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UtilizadorPapel'
CREATE INDEX [IX_FK_UtilizadorPapel]
ON [dbo].[UtilizadorSet]
    ([Papel_Id]);
GO

-- Creating foreign key on [Estado_Id] in table 'ProjectoSet'
ALTER TABLE [dbo].[ProjectoSet]
ADD CONSTRAINT [FK_ProjectoEstado]
    FOREIGN KEY ([Estado_Id])
    REFERENCES [dbo].[EstadoSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ProjectoEstado'
CREATE INDEX [IX_FK_ProjectoEstado]
ON [dbo].[ProjectoSet]
    ([Estado_Id]);
GO

-- Creating foreign key on [Utilizador_Id] in table 'ProjectoSet'
ALTER TABLE [dbo].[ProjectoSet]
ADD CONSTRAINT [FK_UtilizadorProjecto]
    FOREIGN KEY ([Utilizador_Id])
    REFERENCES [dbo].[UtilizadorSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UtilizadorProjecto'
CREATE INDEX [IX_FK_UtilizadorProjecto]
ON [dbo].[ProjectoSet]
    ([Utilizador_Id]);
GO

-- Creating foreign key on [Projecto_Id] in table 'PagamentoSet'
ALTER TABLE [dbo].[PagamentoSet]
ADD CONSTRAINT [FK_ProjectoPagamento]
    FOREIGN KEY ([Projecto_Id])
    REFERENCES [dbo].[ProjectoSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ProjectoPagamento'
CREATE INDEX [IX_FK_ProjectoPagamento]
ON [dbo].[PagamentoSet]
    ([Projecto_Id]);
GO

-- Creating foreign key on [Utilizador_Id] in table 'RegistoDeContactoSet'
ALTER TABLE [dbo].[RegistoDeContactoSet]
ADD CONSTRAINT [FK_UtilizadorContacto]
    FOREIGN KEY ([Utilizador_Id])
    REFERENCES [dbo].[UtilizadorSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_UtilizadorContacto'
CREATE INDEX [IX_FK_UtilizadorContacto]
ON [dbo].[RegistoDeContactoSet]
    ([Utilizador_Id]);
GO

-- Creating foreign key on [Projecto_Id] in table 'RegistoDeContactoSet'
ALTER TABLE [dbo].[RegistoDeContactoSet]
ADD CONSTRAINT [FK_ProjectoContacto]
    FOREIGN KEY ([Projecto_Id])
    REFERENCES [dbo].[ProjectoSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ProjectoContacto'
CREATE INDEX [IX_FK_ProjectoContacto]
ON [dbo].[RegistoDeContactoSet]
    ([Projecto_Id]);
GO

-- Creating foreign key on [Projecto_Id] in table 'ProjectoLogSet'
ALTER TABLE [dbo].[ProjectoLogSet]
ADD CONSTRAINT [FK_ProjectoLogProjecto]
    FOREIGN KEY ([Projecto_Id])
    REFERENCES [dbo].[ProjectoSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ProjectoLogProjecto'
CREATE INDEX [IX_FK_ProjectoLogProjecto]
ON [dbo].[ProjectoLogSet]
    ([Projecto_Id]);
GO

-- Creating foreign key on [PromotorProjecto_Id] in table 'ProjectoSet'
ALTER TABLE [dbo].[ProjectoSet]
ADD CONSTRAINT [FK_ProjectoPromotorProjecto]
    FOREIGN KEY ([PromotorProjecto_Id])
    REFERENCES [dbo].[PromotorProjectoSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_ProjectoPromotorProjecto'
CREATE INDEX [IX_FK_ProjectoPromotorProjecto]
ON [dbo].[ProjectoSet]
    ([PromotorProjecto_Id]);
GO

-- Creating foreign key on [Promotor_Id] in table 'PromotorProjectoSet'
ALTER TABLE [dbo].[PromotorProjectoSet]
ADD CONSTRAINT [FK_PromotorProjectoPromotor]
    FOREIGN KEY ([Promotor_Id])
    REFERENCES [dbo].[PromotorSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_PromotorProjectoPromotor'
CREATE INDEX [IX_FK_PromotorProjectoPromotor]
ON [dbo].[PromotorProjectoSet]
    ([Promotor_Id]);
GO

-- Creating foreign key on [Anexo_Id] in table 'AnexoProjectoSet'
ALTER TABLE [dbo].[AnexoProjectoSet]
ADD CONSTRAINT [FK_AnexoAnexoProjecto]
    FOREIGN KEY ([Anexo_Id])
    REFERENCES [dbo].[AnexoSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AnexoAnexoProjecto'
CREATE INDEX [IX_FK_AnexoAnexoProjecto]
ON [dbo].[AnexoProjectoSet]
    ([Anexo_Id]);
GO

-- Creating foreign key on [AnexoProjecto_Id] in table 'ProjectoSet'
ALTER TABLE [dbo].[ProjectoSet]
ADD CONSTRAINT [FK_AnexoProjectoProjecto]
    FOREIGN KEY ([AnexoProjecto_Id])
    REFERENCES [dbo].[AnexoProjectoSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_AnexoProjectoProjecto'
CREATE INDEX [IX_FK_AnexoProjectoProjecto]
ON [dbo].[ProjectoSet]
    ([AnexoProjecto_Id]);
GO

-- Creating foreign key on [Id] in table 'ProjectoSet_Incentivo'
ALTER TABLE [dbo].[ProjectoSet_Incentivo]
ADD CONSTRAINT [FK_Incentivo_inherits_Projecto]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[ProjectoSet]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'ProjectoSet_Bonificação'
ALTER TABLE [dbo].[ProjectoSet_Bonificação]
ADD CONSTRAINT [FK_Bonificação_inherits_Projecto]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[ProjectoSet]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'AnexoSet_Despacho'
ALTER TABLE [dbo].[AnexoSet_Despacho]
ADD CONSTRAINT [FK_Despacho_inherits_Anexo]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[AnexoSet]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'AnexoSet_Parecer'
ALTER TABLE [dbo].[AnexoSet_Parecer]
ADD CONSTRAINT [FK_Parecer_inherits_Anexo]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[AnexoSet]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating foreign key on [Id] in table 'AnexoSet_Reforco'
ALTER TABLE [dbo].[AnexoSet_Reforco]
ADD CONSTRAINT [FK_Reforco_inherits_Anexo]
    FOREIGN KEY ([Id])
    REFERENCES [dbo].[AnexoSet]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------