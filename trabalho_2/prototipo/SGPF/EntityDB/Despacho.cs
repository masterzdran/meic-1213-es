//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SGPF.EntityDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class Despacho : Anexo
    {
        public decimal LimiteFinanciamento { get; set; }
        public decimal MontanteFinanciamento { get; set; }
        public string ResultadoDaAvaliacao { get; set; }
        public string InfoCustoElegivel { get; set; }
        public string PrazoExecucao { get; set; }
    }
}
