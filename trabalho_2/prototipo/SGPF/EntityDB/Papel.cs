//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SGPF.EntityDB
{
    using System;
    using System.Collections.Generic;
    
    public partial class Papel
    {
        public Papel()
        {
            this.Utilizador = new HashSet<Utilizador>();
        }
    
        public int Id { get; set; }
        public string Valor { get; set; }
    
        public virtual ICollection<Utilizador> Utilizador { get; set; }
    }
}
