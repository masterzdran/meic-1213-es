﻿using System;
using System.Web.Mvc;
using System.Web.Security;
using SGPF.EntityDB;
using SGPF.Services;

namespace SGPF.Controllers
{
    public class ProjectController : Controller
    {
        private static readonly ProjectDaoService Dao = ProjectDaoService.GetInstance();

        [HttpPost]
        public ViewResult Suspend(int id)
        {
            var p = Dao.Read(id);
            
            p.Suspenso = !p.Suspenso;
            Dao.Update(p);

            return View("Details",p);
        }

        [HttpPost]
        public ViewResult Search(String description)
        {
            return View("Index", Dao.GetAll(description));
        }

        //
        // GET: /Project/

        public ViewResult Index()
        {
            return View(Dao.GetAll());
        }

        //
        // GET: /Project/Details/5

        public ViewResult Details(int id)
        {
            Projecto projectmodel = Dao.Read(id);
            return View(projectmodel);
        }

        //
        // GET: /Project/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Project/Create

        [HttpPost]
        public ActionResult Create(Projecto projectmodel)
        {
            if (ModelState.IsValid)
            {
                Dao.Create(projectmodel);
                return RedirectToAction("Index");
            }

            return View(projectmodel);
        }

        //
        // GET: /Project/Edit/5

        public ActionResult Edit(int id)
        {
            Projecto projectmodel = Dao.Read(id);
            return View(projectmodel);
        }

        //
        // POST: /Project/Edit/5

        [HttpPost]
        public ActionResult Edit(Projecto projectmodel)
        {
            if (ModelState.IsValid)
            {
                Dao.Update(projectmodel);
                return RedirectToAction("Index");
            }

            return View(projectmodel);
        }

        //
        // GET: /Project/Delete/5

        public ActionResult Delete(int id)
        {
            return View(Dao.Read(id));
        }

        //
        // POST: /Project/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Dao.Delete(id);
            return RedirectToAction("Index");
        }
    }
}