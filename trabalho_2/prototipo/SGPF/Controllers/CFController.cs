﻿using System.Web.Mvc;

namespace SGPF.Controllers
{
    [Authorize]
    public class CFController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ComissionDispatch()
        {
            return View();
        }

        public ActionResult ReportProjectInfo()
        {
            return View();
        }

        public ActionResult ReportProjectPayment()
        {
            return View();
        }
    }
}