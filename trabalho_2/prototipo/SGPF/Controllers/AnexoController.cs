﻿using System;
using System.Web.Mvc;
using SGPF.EntityDB;
using SGPF.Services;

namespace SGPF.Controllers
{
    public class AnexoController : Controller
    {
        private readonly AnexoDaoService Dao = AnexoDaoService.GetInstance();

        public ViewResult Index()
        {
            return View(Dao.GetAll());
        }

        //
        // GET: /Anexo/Details/5

        public ViewResult Details(int id)
        {
            var a = Dao.Read(id);

            if (User.IsInRole("Tecnico"))
            {
                return View("DetailsParecer", a as Parecer );

            }
            
            if (User.IsInRole("Comissao"))
            {
                return View("DetailsDespacho", a as Despacho);

            }

            return View(a);
        }

        //
        // GET: /Project/Create

        public ActionResult Create()
        {
            if (User.IsInRole("Tecnico"))
            {
                return View("CreateParecer");
            }

            if (User.IsInRole("Comissao"))
            {
                return View("CreateDespacho");
            }

            return View();
        }

        //
        // POST: /Project/Create

        [HttpPost]
        public ActionResult Create(Anexo a)
        {
            if (ModelState.IsValid)
            {
                Dao.Create(a);
                return RedirectToAction("Index");
            }

            return View(a);
        }

        [HttpPost]
        public ActionResult CreateDespacho(Despacho a)
        {
            if (ModelState.IsValid)
            {
                Dao.Create(a);
                return RedirectToAction("Index");
            }

            return View(a);
        }

        [HttpPost]
        public ActionResult CreateParecer(Parecer a)
        {
            if (ModelState.IsValid)
            {
                Dao.Create(a);
                return RedirectToAction("Index");
            }

            return View(a);
        }
    }
}