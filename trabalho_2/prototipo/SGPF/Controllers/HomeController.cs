﻿using System.Web.Mvc;

namespace SGPF.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = "Sistema de gestão de projectos de financiamento";

            return View();
        }

        public ActionResult About()
        {
            return View();
        }
    }
}