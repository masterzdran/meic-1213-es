A organização XYZ pretende a implementação de um sistema de gestão de projectos de
financiamento para apoio à criação de redes de investigação e desenvolvimento (I&D).

Os projectos de financiamento:
	- podem ser Incentivos ou...
	No caso de Incentivos:
		- o pagamento é efectuado em várias prestações.

	- podem ser Bonificações.
	No caso das Bonificações:
		- é definido um período de bonificação, a taxa de bonificação, e o montante máximo de bonificação.
		- Uma bonificação é um subsídio aos juros de um empréstimo, previamente contratado com um banco, para desenvolvimento de um projecto de I&D. A percentagem é relativa ao juro do empréstimo.

O pagamento: 
	- só pode ser efectuado após a verificação do prazo de validade e do limite de financiamento estabelecido para o projecto.
	- Para efeitos de pagamento cada projecto possui associada uma conta bancária, através da qual serão feitos os pagamentos.
	- Quem é responsável por efectuar os pagamentos? É algum utilizador ou é o sistema de forma automática? Quem gere os pagamentos é o gestor de financiamento (introduz no sistema a informação sobre os pagamentos a realizar).

A comissão de financiamento pode:
	- aprovar e o projecto passa à fase de pagamento. Ao ser aprovado um projecto, para além de outra informação, deve ser definido o custo elegível e o montante de financiamento.
	- rejeitar um projecto
	- O que acontece quando um projecto é rejeitado pela comissão de financiamento? É arquivado?
		- Quando um projecto é rejeitado fica no estado "Rejeitado".
	- transformar o projecto em bonificação, devendo o despacho respectivo ser anexado ao projecto
	- A comissão de financiamento autoriza (ou não) o reforço de Financiamento.

Um projecto:
	- pode ser suspenso e reactivado durante as diversas fases do seu desenvolvimento incluindo durante a fase de pagamento. 
	- Após a conclusão dos pagamentos o projecto fica fechado.
	- definido o número do projecto (gerado automaticamente) a data de criação do projecto
	- o técnico a quem foi atribuído. 
	- Posteriormente, deve ser registada a data e hora de todas as decisões que
	ocorram em relação a um projecto.
	- A alteração dos dados de um projecto só é possível enquanto este estiver em análise técnica ou à espera de despacho da comissão de financiamento.
	- deve ser possível realizar o reforço de financiamento de um projecto desde que este esteja em pagamento ou fechado.
	- Nesse caso o projecto é enviado à comissão de financiamento, ficando à espera de despacho.
	- Um projecto rejeitado não pode ser sujeito a qualquer outra acção posterior a não ser consulta.
	 - A suspensão e reactivação dos projectos é da responsabilidade de que utilizador? É da responsabilidade do funcionário que tiver o projecto a seu cargo enquanto activo ou antes da suspensão.

O promotor de um projecto
	- pode ser nacional ou estrangeiro, 
	- existindo uma entidade responsável pelos contactos resultantes do decorrer do processo.
	- indica na candidatura o tipo de projecto. No entanto, para projectos do tipo Incentivo, a comissão de financiamento pode decidir converter um projecto de Incentivo em Bonificação
	- Os processos que estão arquivados podem ser reenquadrados? Quem é responsável por este processo? Sim, o promotor, que deve apresentar nova candidatura.

======================================================
================ outras questões =====================
======================================================

A submissão de candidaturas será online?
	- A entrega de candidaturas é presencial, inserindo o técnico os dados da candidatura no sistema.

Um dos pontos do documento de visão é “key stakeholder/user needs”. Será necessário detalhar este
ponto? É necessário identificar as acções que cada um deles deverá poder fazer sobre o sistema?
	- A identificação das necessidades dos “stakeholders” é um ponto de partida para identificar as funcionalidades necessárias à satisfação dessas necessidades.

Na descrição do problema é referido que caso o parecer do gestor de financiamento não seja favorável
o projecto é arquivado. Este pode ser reenquadrado, voltando à fase de candidatura. É o promotor que
faz este reenquadramento?
	- O reenquadramento é realizado pelo Gestor de Financiamento ou pela Comissão de Financiamento, com base na reformulação do projecto realizada pelo Promotor.

Os despachos e pareceres técnicos anteriores devem serem guardados ou substituídos pelos novos
(manter histórico)?
	- Todo o histórico de um projecto deve ser mantido.

Um projecto que retornou à fase de candidatura para ser reenquadrado deve manter o mesmo
número de projecto e o mesmo técnico ou devem ser novos? Ou por outro lado, esta informação apenas é
registada quando o projecto é aprovado pelo técnico?
	- Quando um projecto é reenquadrado, o número de projecto é mantido, mas o gestor de financiamento pode mudar.

Como é feita a atribuição das candidaturas aos analistas que devem abrir ou arquivar a candidatura?
O analista escolhe ou é o sistema que atribui aleatoriamente?
	- Não existe a noção de "analista". A recepção de uma candidatura é feita por um técnico (escalado para atendimento). Para o sistema não é necessário indicar informação sobre o técnico. No que se refere ao gestor de financiamento, este é indicado no despacho de abertura de projecto.

As alterações dos dados do projecto são ao nível dos dados internos do sistema, ou ao nível dos dados
específicos do projecto, como por exemplo o objectivo do projecto ou as ferramentas utilizadas? Quem é
responsável por realizar estas alterações?
	- As alterações referem-se a informação associada ao projecto. São realizadas pelo funcionário que tiver o projecto a seu cargo no momento da alteração.

Quais os dados que constituem um parecer técnico?
	- Parecer (texto livre) e decisão (aprovar, rejeitar ou transformar em bonificação)

Que informação deve constar num despacho da comissão de financiamento?
	- Deve ter informação referente ao resultado da avaliação e informação referente ao custo elegível, montante de financiamento e prazo de execução (ver descrição geral).

Todos os financiamentos são caracterizados por um custo elegível e um montante de financiamento?
Ou estes dados são apenas para financiamentos do tipo Incentivo? Existe alguma relação entre eles?
	- Sim, a informação referente a custo elegível e montante de financiamento é comum a bonificações e a incentivos.

Os financiamentos do tipo Bonificação têm um montante máximo de bonificação, uma taxa de
bonificação e um período. Existe alguma relação com o custo ou montante de financiamento?
	- Não, estes dados são independentes dos referidos na questão anterior.

Quem são os únicos actores que têm interacção directa com o sistema?
	- O técnico, o gestor de financiamento e a comissão de financiamento.

Há interacção directa entre a comissão de financiamento e o sistema? Ou seja, a comissão de
financiamento interage directamente com o sistema, ou fá-lo através de alguém, como por exemplo, o
gestor de financiamento? 
	- Sim, os membros da comissão de financiamento podem interagir directamente com o sistema.

Quando a comissão de financiamento emite um despacho, regista-o no sistema ou passa a informação
para o gestor de financiamento? Se passa a informação para o gestor, como é efectuada a passagem? por
papel?
	- A comissão de financiamento regista o despacho directamente no sistema.

Que informação é necessária para uma candidatura?
	- Informação referente à identificação do promotor e dados do projecto, nomeadamente, dados referentes ao promotor (designação, nacionalidade), ao responsável por contactos (nome, telefone, e-mail), ao tipo de projecto (incentivo, bonificação), ao montante de financiamento solicitado (ver descrição do problema).

Os dados associados a um projecto de financiamento são: 
	- Designação, nacionalidade e NIF do promotor, NIB da conta bancária associada, responsável por contactos (designação, e-mail, telefone), tipo de projecto (Incentivo ou Bonificação), montante de financiamento solicitado, descrição do projecto (ver descrição do problema).

Os utilizadores podem ver outros perfis? A comissão pode introduzir candidaturas? O Técnico emite pareceres? As combinações possíveis?
	- As funcionalidades/operações possíveis são as do enunciado.

Existem prioridades/precedências? Existem requisitos de performance?
	- Tempo de resposta "decente"

Em relação à documentação: Pretende Ajuda online? Manual de Utilização detalhado de cada fase?
	- Sim em relação à Ajuda. Pode ser um único manual agrupado por tipo de utilizador e respetivas operações.

Quem pode pedir o reforço?
	- O promotor

Como se realiza as acções actualmente da empresa XYZ?
	- Actualmente é um processo manual. Em relação a repositório de dados e registo de utilizadores, pretende-se uma solução completa, sem integração com a actual infra-estrutura da empresa.

Existe algum Sistema de Registo e Configuração de Utilizadores?

A empresa já possui algum Sistema de Repositório de dados?

Licenciamento do Software? O Cliente quer uma solução fechada? No final pretende ficar com o código da aplicação?
	- Estou a comprar um produto. Naturalmente que pretendo que mo instalem e demonstrem em funcionamento.